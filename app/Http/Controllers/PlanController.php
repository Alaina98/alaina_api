<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plan;
class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records=Plan::get();

        return response()->json([
            'success'=>true,
            'description'=>'Records retrieved successfully',
            'data'=>$records

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $records=new Plan;
        $records->name=$request->name;
        $records->num_of_products=$request->num_of_products;
        $records->num_of_services=$request->num_of_services;
        $records->is_template_enabled=$request->is_template_enabled;
        $records->save();
        
        return response()->json([
            'success'=>true,
            'description'=>'Record addedd Successfully',
            'data'=>null

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $records=Plan::find($id);
        return $records;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['records']=Plan::find($id);
        return view('plans.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $records=Plan::where('id',$id)->first();
        if (!$records)
        return response()->json([
            'success'=>false,
            'description'=>'Record not found',
            'data'=>null

         ]);
        $records->name=$request->name;
        $records->num_of_products=$request->num_of_products;
        $records->num_of_services=$request->num_of_services;
        $records->is_template_enabled=$request->is_template_enabled;
        $records->save();
       
        return response()->json([
            'success'=>true,
            'description'=>'Record updated Successfully',
            'data'=>null

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $records=Plan::find($id)->delete;
        return "record deleted successfully";
    }
}
