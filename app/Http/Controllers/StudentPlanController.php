<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentPlan;
use App\Models\User;
use App\Models\Plan;

class StudentPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records=StudentPlan::get();
        if (!$records->count())
        {
            return response()->json([
                'success'=>false,
                'description'=>'Records not found',
                'data'=>null
    
            ]);
        }

        return response()->json([
            'success'=>true,
            'description'=>'Records retrieved successfully',
            'data'=>$records

        ]);

       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('StudentPlans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!User::where('id',$request->user_id)->exists())
        {
            return response()->json([
                'success'=>false,
                'description'=>'User not found',
                'data'=>null
    
            ]);

        }

        if(!Plan::where('id',$request->plan_id)->exists())
        {
            return response()->json([
                'success'=>false,
                'description'=>'Plan not found',
                'data'=>null
    
            ]);

        }
        if(StudentPlan::where('user_id',$request->user_id)->where('plan_id',$request->plan_id)->exists())
        {
            return response()->json([
                'success'=>false,
                'description'=>'User already subscribe to a plan',
                'data'=>null
    
            ]);

        }
        $records=new StudentPlan;
        $records->user_id=$request->user_id;
        $records->plan_id=$request->plan_id;
        $records->expire_at=$request->expire_at;
        $records->price=$request->price;
        $records->save();
        
        return response()->json([
            'success'=>true,
            'description'=>'Record addedd Successfully',
            'data'=>null

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $records=StudentPlan::find($id);
        return $records;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['records']=StudentPlan::find($id);
        return view('StudentPlans.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $records=StudentPlan::where('id',$id)->first();
        if (!$records)
        return response()->json([
            'success'=>false,
            'description'=>'Record not found',
            'data'=>null

         ]);
        $records->name=$request->name;
        $records->num_of_products=$request->num_of_products;
        $records->num_of_services=$request->num_of_services;
        $records->is_template_enabled=$request->is_template_enabled;
        $records->save();
       
        return response()->json([
            'success'=>true,
            'description'=>'Record updated Successfully',
            'data'=>null

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $records=StudentPlan::find($id)->delete;
        return "record deleted successfully";
    }
}
